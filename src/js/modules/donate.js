class Donate {
	constructor($element) {
		this.$element = $element;
		this.$bt = $element.querySelector('.js-bt-donate');
		this.init();
	}

	donate() {
		let $bt = this.$bt;
		alert('Thank you for the donation!');
		$bt.innerHTML = $bt.dataset.donated;
		$bt.disabled = true;
	}

	bind() {
		this.$bt.addEventListener('click', this.donate.bind(this));
	}

	init() {
		this.bind();
	}
}

export default function () {
	let $cards = document.querySelectorAll('.card'),
		instances = [];

	$cards.forEach($element => {
		instances.push(new Donate($element));
	});
}
