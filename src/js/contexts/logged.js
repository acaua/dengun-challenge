import React from 'react';

const event = new Event('updateLogin'),
	donationsEvent = new Event('donations');

export default React.createContext({
	loggedIn: false,
	donations: [],
	watch: that => {
		window.addEventListener('updateLogin', () => {
			that.setState({});
		});
	},
	setLogin: function (s) {
		this.loggedIn = s;
		window.dispatchEvent(event);
		console.clear();
	},
	gotoDonations: function () {
		window.dispatchEvent(donationsEvent);
	}
});
