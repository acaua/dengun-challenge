console.time('Initialize');
import App from './app/index';
import React from 'react';
import ReactDOM from 'react-dom';
import Main from './components/main';

const app = new App();

app.init(function () {
	console.log('%cMantis Starter', 'color: #338656; font: 50px sans-serif;');
	ReactDOM.render(<Main />, document.querySelector('.layout'));
	console.timeEnd('Initialize');
});
