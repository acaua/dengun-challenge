import React from 'react';

export default class extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			locations: []
		};
	}

	componentDidMount() {
		fetch('./api.json', {method: 'GET'}).then(response => {
			return response.json();
		}).then(data => {
			this.setState({
				locations: data.locations
			});
		});
	}

	render() {
		const [
			locations,
			handleLocation,
			handleDate,
			valueLocation,
			valueDate,
			handleName,
			valueName
		] = [
			this.state.locations,
			this.props.handleLocation,
			this.props.handleDate,
			this.props.valueLocation,
			this.props.valueDate,
			this.props.handleName,
			this.props.valueName
		];

		return pug`
			section.filters.grid
				.filters__item.grid__item-sm--12.grid__item-md--4.grid__item-lg--3
					label.filters__label(to='region') Localidade
					select.field-select(value= valueLocation, onChange= handleLocation)#region
						option(value= '') Todas as cidades
						each location, i in locations
							option(value= location, key= i)= location

				.filters__item.grid__item-sm--12.grid__item-md--4.grid__item-lg--2
					label.filters__label(to='date') Data
					select.field-select(value= valueDate, onChange= handleDate)#date
						option(value= 0) ---
						option(value= 1) Mais novo
						option(value= 2) Mais velho

				.filters__item.grid__item-sm--12.grid__item-md--4.grid__item-lg--2
					label.filters__label(to='name') Name
					select.field-select(value= valueName, onChange= handleName)#name
						option(value= 0) ---
						option(value= 1) A-Z
						option(value= 2) Z-A
		`;
	}
}
