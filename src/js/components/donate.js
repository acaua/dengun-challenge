import React from 'react';
import LoggedContext from '../contexts/logged';

export default class extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			donated: false,
			defaultText: 'Doar',
			donatedText: 'Já doou'
		};
	}

	static contextType = LoggedContext;

	get checkDonation() {
		if (this.context.loggedIn && !this.state.donated) {
			return false;
		}
		return 'disabled';
	}

	get buttonText() {
		const state = this.state;
		return (state.donated) ? state.donatedText : state.defaultText;
	}

	handleClick(e) {
		const value = parseInt(prompt('Quantia $:'));

		if (!value) {
			return;
		}

		this.props.incrementDonation(value);
		this.setState({
			donated: true
		});
	}

	componentDidMount() {
		this.context.watch(this);
	}

	render() {
		return pug`
			button.bt.js-bt-donate(disabled= this.checkDonation, onClick= this.handleClick.bind(this))= this.buttonText
		`;
	}
}
