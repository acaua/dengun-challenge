import React from 'react';

export default class extends React.Component {
	checkCurrent(n, current) {
		return (n === current) ? 'pagination__item--active' : '';
	}

	render() {
		const length = this.props.length,
			perPage = this.props.perPage,
			current = this.props.current,
			handlePage = this.props.handlePage,
			pages = Math.ceil(length / perPage);

		return pug`
			section.pagination.grid
				.grid__item-sm
					if current !== 1
						a.pagination__item(
							onClick= handlePage,
							page= (current - 1)
						) Anterior

					- let n = 0;
					while n < pages
						- n++
						a.pagination__item(
							className= this.checkCurrent(n, current),
							onClick= handlePage,
							page= n,
							key= n
						)= n

					if current !== pages
						a.pagination__item(
							onClick= handlePage,
							page= (current + 1)
						) Próxima
		`;
	}
}
