import React from 'react';
import LoggedContext from '../contexts/logged';

export default class extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			menuOpen: false,
			user: '',
			password: ''
		};
		this.user = React.createRef();
		this.password = React.createRef();
	}

	static contextType = LoggedContext;

	toggleMenu(menuOpen) {
		return (menuOpen) ? 'navbar__collapse--active' : '';
	}

	handleToggleMenu() {
		this.setState({
			menuOpen: !this.state.menuOpen
		});
	}

	handleSubmit(e) {
		e.preventDefault();
		if (!!this.user.current.value && !!this.password.current.value) {
			this.context.setLogin(true);
			this.handleToggleMenu();
		}
	}

	handleLogout() {
		this.context.setLogin(false);
	}

	handleDonations() {
		this.context.gotoDonations();
		this.handleToggleMenu();
	}

	componentDidMount() {
		this.context.watch(this);
	}

	render() {
		return pug`
			nav.navbar
				.grid.grid--justify-between.grid--align-center
					a.navbar__brand.grid__item-sm--auto(href='./')
						svg.icon(class="icon--logo")
							use(xlinkHref="./img/sprite.svg#logo")
						| Pet Angel

					button.navbar__menu.grid__item-sm--auto(onClick= this.handleToggleMenu.bind(this))
						svg.icon(class="icon--menu")
							use(xlinkHref="./img/sprite.svg#menu")

					form.navbar__collapse(className= this.toggleMenu(this.state.menuOpen), onSubmit= this.handleSubmit.bind(this), required)
						if this.context.loggedIn
							.navbar__collapse__item
								a.navbar__link(onClick= this.handleDonations.bind(this)) Donativos
							.navbar__collapse__item
								a.navbar__link(onClick= this.handleLogout.bind(this)) Sair

						unless this.context.loggedIn
							.navbar__collapse__item
								input.field-text(ref= this.user, type='text', placeholder='Usuário', required)
							.navbar__collapse__item
								input.field-text(ref= this.password, type='password', placeholder='Senha', required)
							.navbar__collapse__item.grid.grid--justify-between.grid--align-center
								button.bt.bt--large.bt--block Iniciar
		`;
	}
}
