import React from 'react';
import Card from './card';

export default class extends React.Component {
	render() {
		const items = this.props.items;

		return pug`
			section.card-container.grid
				each item in items
					.grid__item-sm--auto.grid__item-md--6.grid__item-lg--4(key = item.id)
						Card(item= item)
		`;
	}
}
