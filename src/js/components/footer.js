import React from 'react';

export default class extends React.Component {
	render() {
		return pug`
			footer.footer
				.grid.grid--justify-between.grid--align-center
					small.footer__copyright.grid__item-sm--auto &copy; 2019
					.footer__links.grid__item-sm--auto
						a.footer__links__item(href='./', target='_blank', title='Facebook')
							svg.icon(class="icon--facebook")
								use(xlinkHref="./img/sprite.svg#facebook")
						a.footer__links__item(href='./', target='_blank', title='Instagram')
							svg.icon(class="icon--instagram")
								use(xlinkHref="./img/sprite.svg#instagram")
						a.footer__links__item(href='./', target='_blank', title='Twitter')
							svg.icon(class="icon--twitter")
								use(xlinkHref="./img/sprite.svg#twitter")
		`;
	}
}
