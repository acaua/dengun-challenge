import React from 'react';
import Donate from './donate';
import LoggedContext from '../contexts/logged';

export default class extends React.Component {
	constructor(props) {
		super(props);
		this.months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
		this.state = {
			item: {}
		};
	}

	static contextType = LoggedContext;

	incrementDonation(value) {
		this.state.item.donation = parseInt(this.state.item.donation) + value;
		this.context.donations.push(this.state.item);
		this.setState({});
	}

	render() {
		const item = this.props.item,
			date = new Date(item.lost),
			lost = `${this.months[date.getMonth()]} ${date.getFullYear()}`;

		this.state.item = item;

		return pug`
			article.card
				header.card__header
					.card__header__bar #{item.name} -- ##{item.id}
						button.bt.bt--small.bt--small.bt--fussy Partilhar
					img.card__cover(src= item.image, alt= item.name)
				.card__content
					.card__row
						.card__content__name Perdido
						.card__content__value= lost
					.card__row
						.card__content__name Dono
						.card__content__value= item.owner
					.card__row
						.card__content__name Localidade
						.card__content__value= item.location
				footer.card__footer
					.card__budget $#{parseInt(item.donation).toLocaleString('pt-BR')}
						span.card__budget__label doados
					Donate(incrementDonation= this.incrementDonation.bind(this))

				a.card__cta.bt.bt--block.bt--large.bt--success(href='tel:' + item.phone, title="Ligar") (Phone) Ligar
		`;
	}
}
