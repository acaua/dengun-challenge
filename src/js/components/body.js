import React from 'react';
import Filters from './filters';
import Cards from './cards';
import Pagination from './pagination';
import LoggedContext from '../contexts/logged';

export default class extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			items: [],
			valueLocation: '',
			currentPage: 1,
			itemsPerPage: 6,
			valueDate: 1,
			valueName: 0,
			donations: false
		};
		this.bind();
	}

	static contextType = LoggedContext;

	filterLocation(items) {
		const location = this.state.valueLocation;
		if (!!location) {
			return items.filter(i => i.location === location);
		}
		return items;
	}

	filterDonations(items) {
		const donations = this.context.donations;
		return (this.state.donations) ? this.filterLocation(donations) : items;
	}

	setPage(n) {
		this.setState({
			currentPage: n
		});
	}

	get sortByDate() {
		return this.state.items.sort((a, b) => new Date(b.lost) - new Date(a.lost));
	}

	get sortByName() {
		return this.state.items.sort((a, b) => {
			if (a.name < b.name) return -1;
			if (a.name > b.name) return 1;
			return 0;
		});
	}

	sortByDateAsc() {
		this.setState({
			items: this.sortByDate,
			valueName: 0
		});
	}

	sortByDateDesc() {
		this.setState({
			items: this.sortByDate.reverse(),
			valueName: 0
		});
	}

	sortByNameAsc() {
		this.setState({
			items: this.sortByName,
			valueDate: 0
		});
	}

	sortByNameDesc() {
		this.setState({
			items: this.sortByName.reverse(),
			valueDate: 0
		});
	}

	handleLocation(e) {
		this.setState({
			valueLocation: e.currentTarget.value,
			currentPage: 1
		});
	}

	handleDate(e) {
		const value = parseInt(e.currentTarget.value);

		switch (value) {
			case 1:
				this.sortByDateAsc();
				break;
			case 2:
				this.sortByDateDesc();
				break;
		}

		this.setState({
			valueDate: value
		});
	}

	handleName(e) {
		const value = parseInt(e.currentTarget.value);

		switch (value) {
			case 1:
				this.sortByNameAsc();
				break;
			case 2:
				this.sortByNameDesc();
				break;
		}

		this.setState({
			valueName: value
		});
	}

	handlePage(e) {
		this.setPage(parseInt(e.currentTarget.getAttribute('page')));
	}

	componentDidMount() {
		fetch('./api.json', {method: 'GET'}).then(response => {
			return response.json();
		}).then(data => {
			this.setState({
				items: data.pets
			});
			this.sortByDateAsc();
		});
	}

	bind() {
		window.addEventListener('donations', () => {
			this.setState({
				donations: true
			});
		});
	}

	render() {
		const state = this.state,
			items = state.items,
			filteredItems = this.filterDonations(this.filterLocation(items)),
			valueLocation = state.valueLocation,
			valueDate = state.valueDate,
			valueName = state.valueName,
			lastIndex = state.currentPage * state.itemsPerPage,
			firstIndex = lastIndex - state.itemsPerPage,
			currentItems = filteredItems.slice(firstIndex, lastIndex);

		return pug`
			main.layout__body
				Filters(
					handleLocation= this.handleLocation.bind(this),
					valueLocation= valueLocation,
					handleDate= this.handleDate.bind(this),
					valueDate= valueDate,
					handleName= this.handleName.bind(this),
					valueName= valueName
				)
				Cards(items= currentItems)
				Pagination(
					length= filteredItems.length,
					perPage= state.itemsPerPage,
					current= state.currentPage,
					handlePage= this.handlePage.bind(this)
				)
		`;
	}
}
